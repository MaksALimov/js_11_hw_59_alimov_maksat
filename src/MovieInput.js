import React, {Component} from 'react';

class MovieInput extends Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.value !== this.props.value;
    }

    render() {
        return (
            <input value={this.props.value} onChange={this.props.onChange}/>
        );
    }
}

export default MovieInput;