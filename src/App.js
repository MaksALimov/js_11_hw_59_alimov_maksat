import React, {Component} from 'react';
import './App.css'
import {nanoid} from "nanoid";
import MovieInput from "./MovieInput";
import Joke from "./jokes/Jokes";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            movie: localStorage.getItem('movieName') || [],
            newMovie: [],
        };
    }

    addMovie = e => {
        e.preventDefault();

        this.setState({
            newMovie: [...this.state.newMovie, {movieTitle: this.state.movie, id: nanoid()}]
        });
    };

    componentDidUpdate() {
        localStorage.setItem('movieName', this.state.movie);
    }

    removeMovie = id => {
        this.setState({newMovie: this.state.newMovie.filter(movie => movie.id !== id)});
    };

    render() {
        return (
            <>
            <Joke/>
            <form onSubmit={this.addMovie} className="wrapper">
                <input
                    type="text"
                    value={this.state.movie}
                    onChange={e => this.setState({movie: e.target.value})}
                />
                <button type="submit">Add</button>
                <p>
                    To watch list
                </p>
                {this.state.newMovie.map((movie, i) => (
                    <div key={i}>
                        <MovieInput
                            value={this.state.newMovie[i].movieTitle}
                            onChange={e => this.state.newMovie.map(movie => {
                                if (movie.id === this.state.newMovie[i].id) {
                                   return this.setState({newMovie: [{movieTitle: e.target.value, id: nanoid()}]})
                                }
                                return movie;
                            })}
                        />
                        <button type="button" onClick={() => this.removeMovie(movie.id)}>Delete</button>
                    </div>
                ))}
            </form>
            </>
        );
    }
}

export default App;