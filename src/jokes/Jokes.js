import './Jokes.css';
import {useEffect, useState} from "react";

const url = 'https://api.chucknorris.io/jokes/random';

const Joke = () => {
    const [joke, setJoke] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);

            if (response.ok) {
                const posts = await response.json();
                setJoke([posts])
            }
        }

        fetchData().catch(e => console.error(e))
    }, []);

    const getNewJoke = () => {
        const fetchData = async () => {
            const response = await fetch(url);
            if (response.ok) {
                const posts = await response.json();
                setJoke([posts])
            }
        }
        fetchData().catch(e => console.error(e));
    }

    return (
        <div className="quote">
            {joke.map((joke, i) => {
                return <p key={i}>{joke.value}</p>
            })}
            <button onClick={getNewJoke}>Get new joke</button>
        </div>
    );
};

export default Joke;
